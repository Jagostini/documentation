# R2Devops documentation

## Description

**docs.r2devops.io** is the documentation for r2devops.io

**r2devops.io** is a collaborative hub of CI & CD jobs which
helps you to quickly build powerful pipelines for your projects.

Each jobs of r2devops.io can be used independently or to create fully **customized pipelines.**
You can use them for any kind of software and deployment type. Each job can be
customized through configuration.

**🙋 Add your own job using the [Contributing guide](https://docs.r2devops.io/public-catalog/contribute#contributing-workflow/)**

## Repository

This mono-repo is about:

* Documentation of r2devops.io

```
.
├── docs            # Documentation sources
├── mkdocs.yml      # Documentation configuration
├── Pipfile         # Pipenv dependency file to build doc
└── Pipfile.lock
```

### How to add or update a job

* Follow the [Contributing guide](https://docs.r2devops.io/public-catalog/contribute#contributing-workflow/)

### How to update the hub documentation

As prerequisites, you need to install following dependencies on your system:

* `python3`
* `pipenv`

#### Clone the repository locally

```shell
git clone git@gitlab.com:r2devops/documentation.git
cd documentation
```

### Run with pipenv

1. Install requirements

Documentation is built using [Mkdocs](https://www.mkdocs.org) and [Material for
Mkdocs](https://squidfunk.github.io/mkdocs-material/){:target="_blank"}.

```shell
pipenv install
```

2. Launch Mkdocs

You can launch mkdocs in order to create a local web server with hot reload to
see your updates in live:

```shell
pipenv run mkdocs serve
```

### Run with Docker

There's a Dockerfile configuration file at the root of the project.
To run locally you just have to build the image by running this:
```shell
docker build -t squidfunk/mkdocs-material .
```

You can now run the image
```shell
 docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material:latest
```

### See your update

See your update in live at [https://localhost:8000](https://localhost:8000)

### How to update hub tools

#### Guidelines

For `python` tools:

* Pylint note >= 9
* Usage of logging
* Usage of argparse when args are required
* [`Format`](https://docs.python.org/3/library/functions.html?highlight=format#format) must be used instead of `%s` or string concatenation with `+`
* Docstring format compliant with [Google styleguide](https://google.github.io/styleguide/pyguide.html#244-decision)

#### Requirements

Each tools have their own `Pipfile` in their folder to manage their
dependencies. You must install `pipenv` to work on them:

```shell
pip install pipenv
```
