# Self-managed

This section describes how to setup your self-managed instance of R2Devops. Two
methods are available:

- Running R2Devops on Kubernetes using a [Helm chart](/self-managed/kubernetes/)
- Running R2Devops on Docker using [Docker-compose](/self-managed/docker-compose/)

!!! info "On-demand"
    Runing a self-managed instance of R2Devops is currently only available
    on-demand. To begin the process, book a demo on this
    [page](https://r2devops.io/pricing).

    We will send you 2 tokens required during setup in a secured channel:

    - The token `REGISTRY_TOKEN`
    - The license key
