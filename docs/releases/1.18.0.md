---
title: 1.18.0 Release
description: > 
    R2Devops 1.18.0 introduces a demonstration dashboard and addresses the grouping of duplicate hardcoded jobs in the dashboard.

date: 2023-09-29
---

# **R2Devops 1.18.0 Release**

!!! info "Docker Image Versions"
    - Backend: `v1.20.0`
    - Frontend: `v1.14.5` 

!!! info "Chart Helm Version"
    `v0.6.0`

## **📺 Demonstration Dashboard**

- We have implemented a demonstration page for the dashboard. This page is accessible both from the home page when unconnected and from within the demo organization when connected. With this new page, you can explore the benefits that the dashboard can bring to your organization.

![Demonstration Dashboard](../../images/dashboard-demo.gif)

## **🔗 Grouping of Duplicate Hardcoded Jobs**

- We now group duplicate hardcoded jobs in the jobs table inside the dashboard if they have the same names.

![Grouping of Duplicate Hardcoded Jobs](../../images/hardcoded-duplicates.gif)

## **🔧 Minor Updates and Fixes**

- Analysis Dashboard: Now considers local templates as hardcoded jobs.
- Analysis Dashboard: When there's no CI/CD on a project, a "Create Pipeline" button is available to redirect to the editor.
- Analysis Dashboard: Clicking on overview alerts now redirects to the related tabs.
- Account: Added a confirmation pop-up when attempting to delete a token.
