---
 
title: 1.1.2 release
description: > 
    Let's take a look to all the improvements made in R2Devops in June 2022!
date: 2022-07-01
---

<p hidden>#more</p>

# **R2Devops 1.1.2 release**

**Here we are again. It’s time to check what’s new in R2Devops!** 

---
## Major improvements

During the last month, our team made some major improvements to the platform. 👇

### Container images tag are now specified as variable.

In order to simplify the job images tag customization, we decided to put the container images tag in a variable. Why? So you can easily update it using the variable IMAGE_TAG! 

We added the variable in the documentation of each job, with the default value. See how it works for [NPM script](https://r2devops.io/_/r2devops-bot/npm_scripts)!

![Image tag variable of WPM Script](../../images/image_tag.png)

### Generate a pipeline without account

**⚠️this feature is currently disabled on R2Devops⚠️**
  

To help all user to test the *Pipeline Generator* feature, we allow you to generate a pipeline for a **public project** without connecting to R2Devops. You can easily try it from the [Home page](https://r2devops.io/) or the [*Pipeline Generator*](https://pipeline.r2devops.io/)’s one.

![Pipeline Generator on R2Devops Home Page](../../images/Pipeline_Generator_home.png)

The best part? We selected 4 open source projects you can try *Pipeline Generator* on if you don’t have any public project!

### The light *Pipeline Generator* now generates a full pipeline 

**⚠️this feature is currently disabled on R2Devops⚠️**
  
At the beginning, we thought of letting every user access a light version of the *Pipeline Generator*. Only the jobs of the 2 first stages of the pipeline would be displayed. But we change our mind, and decided to display the full pipeline (you are welcome)!

### Access the home page of R2Devops even when you are connected

To ease the access to the *Pipeline Generator*, we decided to restore the access to the home page when you are logged-in. It means that, if you click on the R2 robot on the top left of the navigation panel, you’ll arrive to the home page, and not your *Project Dashboard* anymore.

### The user needs to fill his GitLab or GitHub token to display his projects

In order to only display your projects and when you want them to be displayed, we need you to fill your account’s token on your **Settings**. As long as you don’t, your **Projects** dashboard will stay empty.

### Get notified when your pipeline can be updated in your project dashboard

We activated the *Pipeline Updates* feature for every license, including the CORE one! 

!!! info

    You have nothing to do to access this feature. The button to generate a pipeline will turn orange when an update is available for one of your project. 

![Pipeline Updates feature display](../../images/Pipelines_updates.png)

### Benefit all R2Devops features freely for up to 5 projects in CORE license

This month, we upgraded the license. Now with a CORE license (meaning you are just registered to R2Devops, you don’t pay anything), you can generate and are warned when you can update your pipeline for **5 public or private projects**!

![CORE license in R2Devops](../../images/core_license.png)

## Minor improvements

Because not only the huge things deserve to be noted and appreciate, you can take a look at the minor changes made this month. 👇

### Create your account in 2 clicks after generating a pipeline

When you generate a pipeline on the *home page* or the *Pipeline Generator* one, a toaster is displayed with a link to sign in. You’ll be automatically redirected to the *sign-in* page, where you can create an account with GitLab, GitHub or via email! 

[Sign in R2Devops toaster](../../images/toaster.png)

### Give feedback on the pipeline created with Pipeline Generator

**⚠️this feature is currently disabled on R2Devops⚠️**
  

You can now easily give us feedback on the pipeline generated using R2Devops. How? Just fill the form displayed below the code! Our team will be notified and will take a look to what went wrong, in order to improve the generation process.

### Change BASIC license name into PREMIUM

It’s just a change of words, the content of the license stays the same. BASIC license is now PREMIUM!

![PREMIUM license in R2Devops](../../images/premium_license.png)

### Improvements on Pipeline Generator

**⚠️this feature is currently disabled on R2Devops⚠️**
  
This month, in order to make the **Pipeline generator** better, we boosted its performance optimization to handle larger projects and generate a pipeline for them too.
We also improve the pipeline generation for JavaScript projects.

### Job’ updates and new jobs

During the past month, we updated many official jobs of R2Devops. Among them:

* [Prettier_check](https://r2devops.io/jobs/tests/prettier_check/)
* [Netlify_deploy](https://r2devops.io/_/r2devops-bot/netlify_deploy)
* [Eslint](https://r2devops.io/jobs/tests/eslint/)
* [Ansible playbook](https://r2devops.io/jobs/deploy/ansible_playbook/)
* [API Doc](https://r2devops.io/jobs/build/apidoc/)
* [Artisan_migrate](https://r2devops.io/jobs/deploy/artisan_migrate/)
* [AWS_s3_sync](https://r2devops.io/jobs/deploy/aws_s3_sync/) 
* [Cargo_build](https://r2devops.io/jobs/build/cargo_build/)
* [Cargo_clippy](https://r2devops.io/jobs/tests/cargo_clippy/)
* [Cargo_doc](https://r2devops.io/jobs/build/cargo_doc/)
* [Composer_install](https://r2devops.io/jobs/.pre/composer_install/)
* [Cypress_run](https://r2devops.io/jobs/tests/cypress_run/)
* [Deployer](https://r2devops.io/jobs/deploy/deployer/)
* [Docker_build](https://r2devops.io/jobs/build/docker_build/)
* [Docusaurus_build](https://r2devops.io/jobs/build/docusaurus_build/)
* [Dotnet_build](https://r2devops.io/jobs/build/dotnet_build/)
* [Doxygen](https://r2devops.io/jobs/build/doxygen/)
* [Dusk_test](https://r2devops.io/jobs/tests/dusk_test/)
* [GitLab-terraform_apply](https://r2devops.io/jobs/deploy/gitlab-terraform_apply/)
* [GitLab-terraform_plan](https://r2devops.io/jobs/provision/gitlab-terraform_plan/)
* [GitLeaks](https://r2devops.io/jobs/tests/gitleaks/)
* [Go_unit_test](https://r2devops.io/jobs/tests/go_unit_test/)
* [Gofmt](https://r2devops.io/jobs/tests/gofmt/)
* [Golint](https://r2devops.io/jobs/tests/golint/)
* [Gradle_build](https://r2devops.io/jobs/build/gradle_build/)
* [Gradle_sonarqube](https://r2devops.io/jobs/tests/gradle_sonarqube/)
* [Gradle_test](https://r2devops.io/jobs/tests/gradle_test/)
* [Gulp](https://r2devops.io/jobs/others/gulp/)
* [Hugo](https://r2devops.io/jobs/build/hugo/)
* [Jest](https://r2devops.io/jobs/tests/jest/)
* [Junit_test](https://r2devops.io/jobs/tests/junit_test/)
* [Kubectl_deploy](https://r2devops.io/jobs/deploy/kubectl_deploy/)
* [Kustomize_deploy](https://r2devops.io/jobs/deploy/kustomize_deploy/)
* [Lighthouse](https://r2devops.io/jobs/tests/lighthouse/)
* [Links_checker](https://r2devops.io/jobs/tests/links_checker/)
* [Marp](https://r2devops.io/jobs/build/marp/)
* [Maven_build](https://r2devops.io/jobs/build/maven_build/)
* [Maven_test](https://r2devops.io/jobs/tests/maven_test/)
* [Mdbook_build](https://r2devops.io/jobs/build/mdbook_build/)
* [Mega_linter](https://r2devops.io/jobs/tests/mega_linter/)
* [Mkdocs](https://r2devops.io/jobs/build/mkdocs/)
* [Newman](https://r2devops.io/jobs/tests/newman/)
* [Ng_build](https://r2devops.io/jobs/build/ng_build/)
* [Ng-lint](https://r2devops.io/jobs/tests/ng_lint/)
* [Ng_test](https://r2devops.io/jobs/tests/ng_test/)
* [Nmap](https://r2devops.io/jobs/tests/nmap/)
* [Npm_build](https://r2devops.io/jobs/build/npm_build/)
* [Npm_install](https://r2devops.io/jobs/.pre/npm_install/)
* [Npm_lint](https://r2devops.io/jobs/tests/npm_lint/)
* [Npm_scripts](https://r2devops.io/jobs/others/npm_scripts/)
* [Npm_test](https://r2devops.io/jobs/tests/npm_test/)
* [Openapi](https://r2devops.io/jobs/build/openapi/)
* [Owasp_dependecy_check](https://r2devops.io/jobs/tests/owasp_dependency_check/)
* [Pages](https://r2devops.io/jobs/deploy/pages/)
* [Php_security_checker](https://r2devops.io/jobs/tests/php_security_checker/)
* [Phpdocumentor](https://r2devops.io/jobs/build/phpdocumentor/)
* [Phpunit_test](https://r2devops.io/jobs/tests/phpunit_test/)
* [Pylint](https://r2devops.io/jobs/tests/pylint/)
* [Python_test](https://r2devops.io/jobs/tests/python_test/)
* [Python_tox](https://r2devops.io/jobs/tests/python_tox/)
* [Semantic_release](https://r2devops.io/jobs/release/semantic_release/)
* [Serverless_deploy](https://r2devops.io/jobs/deploy/serverless_deploy/)
* [Sls_scan](https://r2devops.io/jobs/tests/sls_scan/)
* [Spell_check](https://r2devops.io/jobs/tests/spell_check/)
* [Sphinx_build](https://r2devops.io/jobs/build/sphinx_build/)
* [Ssh](https://r2devops.io/jobs/deploy/ssh/)
* [Super_linter](https://r2devops.io/jobs/tests/super_linter/)
* [Testssl](https://r2devops.io/jobs/tests/testssl/)
* [Trivy_dependency](https://r2devops.io/jobs/tests/trivy_dependency/)
* [Trivy_image](https://r2devops.io/jobs/tests/trivy_image/)
* [Twig_lint](https://r2devops.io/jobs/tests/twig_lint/)
* [Typescript_compile](https://r2devops.io/jobs/build/typescript_compile/)
* [Yarn_build](https://r2devops.io/jobs/build/yarn_build/)
* [Yarn_install](https://r2devops.io/jobs/.pre/yarn_install/)
* [Yarn_lint](https://r2devops.io/jobs/tests/yarn_lint/)
* [Yarn_test](https://r2devops.io/jobs/tests/yarn_test/)
* [Zaproxy](https://r2devops.io/jobs/tests/zaproxy/)
* [Heroku_deploy](https://r2devops.io/jobs/deploy/heroku_deploy/)
