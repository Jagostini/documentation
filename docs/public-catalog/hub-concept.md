---
title: Hub concept
description: Description of R2Devops Hub concept
---

# Concept

![hub overview](../images/Concept-R2.png)

The **R2Devops hub** is a collaborative hub of CI & CD
**ready to use** jobs which helps you to easily build powerful Pipelines for
your projects.

!!! info
    Currently, the hub is focused to provide only **Gitlab 🦊** jobs. We plan
    to support more CI/CD platforms in the future.

Each Job of the hub can be used independently to create fully **customized pipelines.**
You can use them for any kind of software and deployment type. Each job can be
customized through configuration.

Some jobs are **plug and play**, it means that you can used them without
configuration: you just have to include them and they'll work!
