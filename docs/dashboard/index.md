The dashboard contains analysis of your projects' CI/CD configuration files.
It currently scan the HEAD of every repositories.

The goal is to easily enhance, secure and maintain your CI/CD configuration files across your organization.

## **Overview**

The first page of the dashboard is the overview page. It gives you a quick overview of your projects inside the current organization.

Inside this page we can find two global scores.

![Global scores](../images/global_scores.png)

### 🛡️ **Security score**

The security score is calculated based on the following criteria:

- Visibility and Protection of CI/CD varibles used in the CI/CD configuration file (_see Security/Variables tab_)
- Secrets leaks inside the CI/CD configuration file, we also scan the merged configuration (_see Security/Secrets tab_)

### 🏆 **Maintainability score**

The maintainability score is calculated based on the following criteria:

- Number of projects using reusable resources, like templates and R2Devops templates
- Number of projects using up-to-date CI/CD resources (_see Job Usage tab_)

#### Inside projects table

The maintainability status displayed on the projects table is based on the composition of the CI/CD configuration file, it's calculated based on the following criteria:

- Compliant templates and template usage
- Hardcoded jobs and numbers of lines

### 🤿 **More in depth**

#### Weight of each criteria

Each criteria has a weight, which is used to calculate the global score.
Which means that some criteria are more important than others.

#### Penalty

Since some criteria should drastically reduce the score, we added a penalty system.
It means that if a criteria is not respected, the score will be reduced by a certain percentage.

Current criteria with penalty:

- Secrets leaks inside the CI/CD configuration file
